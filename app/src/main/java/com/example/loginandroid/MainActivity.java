package com.example.loginandroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button btnLogin;
    EditText edtName, edtPass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Anhxa();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mimap(edtName.getText().toString(), edtPass.getText().toString());
            }
        });
    }

    private void Anhxa() {
        btnLogin    = (Button) findViewById(R.id.btnLogin);
        edtName     = (EditText) findViewById(R.id.edtName);
        edtPass     = (EditText) findViewById(R.id.edtPass);
    }

    private void mimap(String Name, String Pass){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = null;
        if ((Name.equals("Admin"))&&(Pass.equals("1234"))){
            fragment = new Fragment1();
        }else {
            fragment = new Fragment2();
        }
        fragmentTransaction.replace(R.id.fragmentcontext, fragment);
        fragmentTransaction.commit();
    }
}
